<?php
namespace Sphere\Data\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;

/**
 * @author Waqas Imam <waqas@bargoventures.com>
 */
class Model extends Eloquent
{

}
