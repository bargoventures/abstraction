<?php
namespace Sphere\Data\Contracts;

/**
 * @author Waqas Imam <waqas@bargoventures.com>
 */
interface RepositoryContract {

    public function findById($id, $refresh = false);

    public function findByAll($pagination = false, $perPage = 10);

    public function create(array $data = []);

    public function update(array $data = []);

    public function deleteById($id);

}
