<?php

namespace Sphere\Helpers;
use Hashids\Hashids;

/**
 * @author Waqas Imam <waqas@bargoventures.com>
 */
class Helper {
    public static function hashEncode($id, $salt = 'laravel-project', $length = 10) {
        $hash = new Hashids(config('sphere.hashid.salt'), config('sphere.hashid.length'));
        return $hash->encode($id);
    }

    public static function hashDecode($id, $salt = 'laravel-project', $length = 10) {
        $hash = new Hashids(config('sphere.hashid.salt'), config('sphere.hashid.length'));
        return !empty($hash->decode($id))? $hash->decode($id)[0] : null;
    }   
}